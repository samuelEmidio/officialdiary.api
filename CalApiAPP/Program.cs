﻿



using System.Net.Http.Headers;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        Task.Run(() => MainAsync());
        Console.ReadLine();
    }

    static async Task<string> MainAsync()
    {
        using (var client = new HttpClient())
        {
            client.BaseAddress = new Uri("https://migracao.iusnatura.com/cal4-02-api");
            var byteArray = new UTF8Encoding().GetBytes("buildit@iusnatura.com.br:j]DTt2/t");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials"),
                new KeyValuePair<string, string>("scope", "all")
            });
            var result = await client.PostAsync("/token", content);
            string resultContent = await result.Content.ReadAsStringAsync();
            Console.WriteLine(resultContent);
            return resultContent;
        }
    }
}

