﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FunctionIbmcloud
{
    public class Attachment
    {
        [JsonProperty("content_type")]
        public string ContentType { get; set; }
        public string Revpos { get; set; }
        public string Digest { get; set; }
        public int Length { get; set; }
        public bool Stub { get; set; }
    }
}
