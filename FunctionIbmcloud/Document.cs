﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FunctionIbmcloud
{
    public class Document : BaseEntity
    {
        [JsonProperty("_attachments")]
        public Dictionary<string, Attachment> Attachments;

    }
}
