﻿using MyCouch;
using MyCouch.Net;
using MyCouch.Requests;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace FunctionIbmcloud
{
    public class Hello
    {
        public JObject Main(JObject args)
        {

            JObject message = new JObject();

            Console.WriteLine("Processo Iniciado");

            SendDocumentToCouch().Wait();

            Console.WriteLine("Processo Finalizado");

            return (message);
        }


        public async Task<bool> SendDocumentToCouch()
        {
            DbConnectionInfo dbConnectionInfo = new DbConnectionInfo("https://apikey-v2-rixfdlnxzay10itpbgmhoq6owbe81k3p9bp4cosegvx:aeb25e59c98560b144c00e6d992b2d65@f637a06f-2a43-4708-9e5e-a7fa7dc23ec8-bluemix.cloudantnosqldb.appdomain.cloud", "database_ius_natura_dev");
            dbConnectionInfo.Timeout = TimeSpan.FromMilliseconds(System.Threading.Timeout.Infinite);

            MyCouchClient client = new MyCouchClient(dbConnectionInfo);

            var officialDiary = new OfficialDiary()
            {
                Active = true,
                CreateAt = DateTime.Now,
                UpdateAt = DateTime.Now,
                Type = DoTypeEnum.Dou
            };

            var json = client.DocumentSerializer.Serialize(officialDiary);
            var document = await client.Documents.PostAsync(json);




            var text = System.IO.File.ReadAllText(AppContext.BaseDirectory + @"/2021-03-01_1.pdf.txt",Encoding.UTF8);
            
            var base64 = EncodeTo64(text);

            var bytes = System.Convert.FromBase64String(base64);

            var requestAttachment = new PutAttachmentRequest(
                            document.Id, document.Rev,
                            "Official Diary",
                            HttpContentTypes.Jpeg,
                            bytes);
            var attachment = await client.Attachments.PutAsync(requestAttachment);

            return true;

        }



        private byte[] fakeFile()
        {
            byte[] bytes = null;
            using (var ms = new MemoryStream())
            {
                using (TextWriter tw = new StreamWriter(ms))
                {
                    tw.Write("texto teste sem razao alguma");
                    tw.Flush();
                    ms.Position = 0;
                    bytes = ms.ToArray();
                }
                return bytes;
            }
        }

        private string EncodeTo64(string toEncode)
        {

            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.UTF8.GetBytes(toEncode);

            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;

        }



    }
}
