﻿using Microsoft.Extensions.Configuration;
using MyCouch;
using OfficialDiary.Domain.Core.Entities;
using OfficialDiary.Domain.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Infra.Data.DatabaseContext
{
    public class CouchDbContext
    {
        private readonly IConfiguration _config;
        private IList<MyCouchClient> _couchClients;

        public CouchDbContext(IConfiguration config)
        {
            _config = config;
            _couchClients = new List<MyCouchClient>();
        }


        public async Task<MyCouchClient> GetRepo(Type entity, String db)
        {
            var strConnection = _config.GetSection("Cloudant").GetSection("Link").Value;


            if (entity.FullName == new OfficialDiaryParse().GetType().FullName) {
                return await GetClient(strConnection, db);
            }

            if (entity.FullName == new Domain.Entities.OfficialDiary().GetType().FullName)
            {
                return await GetClient(strConnection, db);
            }

            return null;
        }


        private async Task<MyCouchClient> GetClient(string strConnection,string database)
        {

            var client = _couchClients.FirstOrDefault(x => x.Connection.DbName == database);
            if(client is null)
            {
                var timout = TimeSpan.FromMilliseconds(System.Threading.Timeout.Infinite);
                DbConnectionInfo dbConnectionInfo = new DbConnectionInfo(strConnection, database.ToLower());
                dbConnectionInfo.Timeout = timout;
                var myCouchClient = new MyCouchClient(dbConnectionInfo);
                var result = await myCouchClient.Database.PutAsync();
                _couchClients.Add(myCouchClient);
                return myCouchClient;
            }
            return client;
        }

    }


  

}
