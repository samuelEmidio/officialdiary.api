﻿using CouchDB.Driver.Types;
using Microsoft.Extensions.Configuration;
using MyCouch;
using OfficialDiary.Domain.Entities;
using OfficialDiary.Domain.Interfaces.Repositories;
using OfficialDiary.Infra.Data.DatabaseContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Infra.Data.Repositories
{
    public class OfficialDiaryRepository : BaseRepository<Domain.Entities.OfficialDiary>, IOfficialDiaryRepository
    {
        private readonly IConfiguration _config;
        public OfficialDiaryRepository(CouchDbContext context,
            IConfiguration configuration) : base(context)
        {
            _config = configuration;
            var database = _config.GetSection("Cloudant").GetSection("DatabaseOfficialDiary").Value;
            SetRepo(database).Wait();
        }
    } 
}

