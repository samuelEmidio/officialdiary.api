﻿using OfficialDiary.Domain.Interfaces.Repositories;
using MyCouch;
using OfficialDiary.Domain.Entities;
using OfficialDiary.Infra.Data.DatabaseContext;
using OfficialDiary.Infra.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using CouchDB.Driver.Types;
using MyCouch.Requests;

namespace OfficialDiary.Infra.Data.Repositories
{
    public class ParseOfficialDiaryRepository : BaseRepository<OfficialDiaryParse>, IParseOfficialDiaryRepository
    {
        public ParseOfficialDiaryRepository(CouchDbContext context) : base(context)
        {
            
        }

        

    }
}
