﻿using OfficialDiary.Infra.Data.DatabaseContext;
using MyCouch;
using MyCouch.Requests;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficialDiary.Domain.Interfaces.Repositories;
using OfficialDiary.Domain.Core.Entities;
using MyCouch.Responses;

namespace OfficialDiary.Infra.Data.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : Document
    {

        protected readonly Dictionary<String, MyCouchClient> _couchClientList = new Dictionary<string, MyCouchClient>();

        protected MyCouchClient _couchClient;

        protected readonly CouchDbContext _context;

        protected String _db;

        public BaseRepository(CouchDbContext context)
        {
            _context = context;
            _db = "";
            _couchClient = null;
        }

        public async Task SetRepo(String db) {
            _db = db;
            
            if(_couchClientList.ContainsKey(db)){
                _couchClient = _couchClientList[db];
            }else {
                MyCouchClient couchClient = await _context.GetRepo(typeof(TEntity), db);
                _couchClientList.Add(_db, couchClient);
                _couchClient = couchClient;
            }
            
        }

        public async Task<bool> Add(TEntity obj)
        {
            var json = _couchClient.DocumentSerializer.Serialize(obj);
            var result = await _couchClient.Documents.PostAsync(json);
            return result.IsSuccess;
        }


        public async Task<TEntity> GetById(string id)
        {
            var result = await _couchClient.Entities.GetAsync<TEntity>(id);
            return result.Content;

        }

        public async Task<bool> Remove(string id)
        {
            var entity = await GetById(id);
            var result = await _couchClient.Entities.DeleteAsync(entity);
            return result.IsSuccess;
        }

        public async Task<bool> Update(TEntity obj)
        {
            var json = _couchClient.DocumentSerializer.Serialize(obj);
            var result = await _couchClient.Entities.PutAsync(json);
            return result.IsSuccess;
        }

        public async Task<AttachmentResponse> GetAttachment(string docId, string docName)
        {
            var result = await _couchClient.Attachments.GetAsync(docId, docName);
            return result;
        }


        public async Task<bool> BulkInsert(List<TEntity> obj,int package = 100)
        {
            var chunk = obj.Chunk(package);

            for (int i = 0; i < chunk.Count(); i++)
            {
                var ch = chunk.ElementAt(i);

                var request = new BulkRequest();
                foreach (var item in ch)
                {
                    var json = _couchClient.DocumentSerializer.Serialize(item);
                    request.Include(json);
                }
                var result = await _couchClient.Documents.BulkAsync(request);
            }

            return true;
        }


        public async void AddCallback(GetChangesRequest getChangesRequest, Action<string> callback)
        {
            var cancellation = new CancellationTokenSource();
            await _couchClient.Changes.GetAsync(getChangesRequest,
              data => {
                  if (data != "")
                  {
                      callback(data.ToString());
                  }

              },
                cancellation.Token);

            cancellation.Cancel();
        }

        public void Dispose()
        {
            
        }
    }



}
