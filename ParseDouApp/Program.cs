﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ParseDouApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\Users\Samuel Emidio\Downloads\output\2021-03-01_2.pdf.txt";

            teste3(path);

            //var text = System.IO.File.ReadAllText(@"C:\Users\Samuel Emidio\Downloads\output\2021-03-01_1.pdf.txt",Encoding.UTF8);

            //string exmple = "ATO Nº 1.249, DE 25 DE FEVEREIRO DE 2021";//etc
            //Regex regex = new Regex("(Nº\\s\\d+?\\.\\d+,\\sDE\\s\\d.\\sDE\\s[A-Z]+\\w\\sDE\\s\\d+\\n)");

            //Regex regex2 = new Regex("(Nº\\s\\d,\\sDE\\s\\d+\\s)");

            //Regex regex3 = new Regex("(Nº\\s\\d?\\d,\\sDE\\s\\d+\\s)");

            //var teste = regex.Split(text);

            //int i = 0;
            //foreach (var t in text)
            //{
            //    var line = t.Replace("Ç", "C").Replace("ç","c");
            //    line += "\n";

            //    bool check = regex.IsMatch(line) || regex2.IsMatch(line);

            //    if (check)
            //    {
            //        Console.WriteLine("Opa");
            //    }
            //    i++;
            //}





            var test = "";


        }

        public static void teste2(string path)
        {
            var text = System.IO.File.ReadAllLines(path);

            Regex regex = new Regex("(Nº\\s\\d+?\\.\\d+,\\sDE\\s\\d.\\sDE\\s[A-Z]+\\w\\sDE\\s\\d+\\n)");

            Regex regex2 = new Regex("(Nº\\s\\d?\\d,\\sDE\\s\\d+\\s)");

            Regex regex3 = new Regex("(([^\\n\\r\\.º;]+)\\n){1,3}([\\s/A-ZÇÃÕÁÉÍÓÚÂÊÎÔÛ-]+)?((Nº)?\\s(\\d+\\.?(\\d+)?)(,\\s)(DE\\s)(\\d+))");

            Dictionary<string, List<string>> keyValuePairs = new Dictionary<string, List<string>>();

            var key = "no key";
            int index = 0;
            foreach (string str in text)
            {
                var line = str; //str.Replace("Ç", "C").Replace("ç", "c");
                line += "\n";

                bool isEpigrafe = regex3.IsMatch(line);// || regex2.IsMatch(line);

                if (isEpigrafe)
                {
                    key = line;
                    if (!keyValuePairs.ContainsKey(key))
                        keyValuePairs.Add(key, new List<string>());
                }
                else
                {
                    if (keyValuePairs.ContainsKey(key))
                        keyValuePairs[key].Add(line);
                    else
                    {
                        keyValuePairs.Add(key, new List<string>());
                    }

                }
                index++;
            }
            var teste = "";

        }


        public static void teste3(string path)
        {
            var text = System.IO.File.ReadAllText(path,Encoding.UTF8);

            var split = Regex.Matches(text, @"(?<orgao1>([^\n\r\.º;]+)\n)?(?<orgao2>([^\n\r\.º;]+)\n)?(?<orgao3>([^\n\r\.º;]+)\n)(?<tipo>[ /A-ZÇÃÕÁÉÍÓÚÂÊÎÔÛ-]+)?(?<norma>(Nº)?\s(?<numeronorma>\d+\.?(\d+)?),\sDE\s(?<datanorma>\d+.+))");

            //var splitTeste = Regex.Split(text, @"(?<orgao1>([^\n\r\.º;]+)\n)?(?<orgao2>([^\n\r\.º;]+)\n)?(?<orgao3>([^\n\r\.º;]+)\n)(?<tipo>[ /A-ZÇÃÕÁÉÍÓÚÂÊÎÔÛ-]+)?(?<norma>(Nº)?\s(?<numeronorma>\d+\.?(\d+)?),\sDE\s(?<datanorma>\d+.+))");

            //int indexContent = 0;
            //int i = 0;
            //foreach (Match match in split)
            //{

            //    GroupCollection groups = match.Groups;

            //    var orgao1 = groups["orgao1"];
            //    var orgao2 = groups["orgao2"];
            //    var orgao3 = groups["orgao3"];
            //    var tipo = groups["tipo"];
            //    var norma = groups["normal"];
            //    var numeroNorma = groups["numeronorma"];
            //    var dataNorma = groups["dataNorma"];

            //    var inicioContent = match.Index + match.Length;

            //    var texto = text.Substring(match.Index, match.Length);


            //    indexContent = match.Index;

            //    i++;
            //}

            string pathFile = @"c:\temp\2021-03-01_2.txt";
            if (!System.IO.File.Exists(pathFile))
            {
                // Create a file to write to.
                using (System.IO.StreamWriter sw = System.IO.File.CreateText(pathFile))
                {

                    for (int x = 0; x < split.Count; x++)
                    {
                        GroupCollection groups = split[x].Groups;

                        var all = groups[0];

                        var orgao1 = groups["orgao1"];
                        var orgao2 = groups["orgao2"];
                        var orgao3 = groups["orgao3"];
                        var tipo = groups["tipo"];
                        var norma = groups["norma"];
                        var numeroNorma = groups["numeronorma"];
                        var dataNorma = groups["datanorma"];

                        var startContent = split[x].Index + split[x].Length;

                        var endContent = x + 1 < split.Count ? split[x + 1].Index : text.Length;

                        var content = text.Substring(startContent, (endContent - startContent));

                        sw.WriteLine("");
                        sw.WriteLine("");
                        sw.WriteLine("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ");

                        sw.WriteLine("all: " + all);
                        sw.WriteLine("orgao1: " + orgao1);
                        sw.WriteLine("orgao2: " + orgao2);
                        sw.WriteLine("orgao3: " + orgao3);
                        sw.WriteLine("tipo: " + tipo);
                        sw.WriteLine("norma: " + norma);
                        sw.WriteLine("numero norma: " + numeroNorma);
                        sw.WriteLine("data norma: " + dataNorma);

                        sw.WriteLine("CONTENT: ");
                        sw.WriteLine(content);



                        sw.WriteLine("");
                        sw.WriteLine("");
                    }





                }
            }


        }

    }
}
