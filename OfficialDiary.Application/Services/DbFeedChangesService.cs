﻿using OfficialDiary.Domain.Interfaces.Services;
using MyCouch;
using MyCouch.Requests;
using MyCouch.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficialDiary.Infra.Data.DatabaseContext;
using OfficialDiary.Domain.Interfaces.Repositories;

namespace OfficialDiary.Application.Services
{
    public class DbFeedChangesService : IDbFeedChangesStartService
    {
        private readonly IParseOfficialDiaryRepository _parseOfficialDiaryRepository;
        private readonly IOfficialDiaryRepository _officialDiaryRepository;
        private IOfficialDiaryService _iOfficialDiaryService;

        public DbFeedChangesService(IParseOfficialDiaryRepository parseOfficialDiaryRepository,
            IOfficialDiaryRepository officialDiaryRepository,
            IOfficialDiaryService officialDiaryService)
        {
            _parseOfficialDiaryRepository = parseOfficialDiaryRepository;
            _officialDiaryRepository = officialDiaryRepository;
            _iOfficialDiaryService = officialDiaryService;
        }

        public void RegisterChanges()
        {

            _officialDiaryRepository.AddCallback(new GetChangesRequest
            {
                Feed = ChangesFeed.Continuous,
                Since = "now",
                Heartbeat = 1000,
                Timeout = Timeout.Infinite,
                Style = ChangesStyle.MainOnly,
                IncludeDocs = true,
            }, (data) => _iOfficialDiaryService.ChangeDocument(data));
        }
    }
}
