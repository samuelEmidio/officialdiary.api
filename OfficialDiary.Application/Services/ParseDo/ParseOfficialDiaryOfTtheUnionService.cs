﻿
using OfficialDiary.Domain.Entities;
using OfficialDiary.Domain.Enum;
using OfficialDiary.Domain.Interfaces.Services.ParseDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OfficialDiary.Application.Services.ParseDo
{
    public class ParseOfficialDiaryOfTtheUnionService : IParseOfficialDiaryService
    {
        public DoTypeEnum DoType { get; set; } = DoTypeEnum.Dou;

        public List<OfficialDiaryParse> DoParse(string input)
        {

   
            List<OfficialDiaryParse> listOfficialDiarys = new List<OfficialDiaryParse>();
            var text = input;//System.IO.File.ReadAllText(path);

            var split = Regex.Matches(text, @"(?<orgao1>([^\n\r\.º;]+)\n)?(?<orgao2>([^\n\r\.º;]+)\n)?(?<orgao3>([^\n\r\.º;]+)\n)(?<tipo>[ /A-ZÇÃÕÁÉÍÓÚÂÊÎÔÛ-]+)?(?<norma>(Nº)?\s(?<numeronorma>\d+\.?(\d+)?),\sDE\s(?<datanorma>\d+.+))");

            for (int x = 0; x < split.Count; x++)
            {
                GroupCollection groups = split[x].Groups;

                var all = groups[0];

                var orgao1 = groups["orgao1"];
                var orgao2 = groups["orgao2"];
                var orgao3 = groups["orgao3"];
                var tipo = groups["tipo"];
                var norma = groups["norma"];
                var numeroNorma = groups["numeronorma"];
                var dataNorma = groups["datanorma"];

                var startContent = split[x].Index + split[x].Length;

                var endContent = x + 1 < split.Count ? split[x + 1].Index : text.Length;

                var content = text.Substring(startContent, (endContent - startContent));

                OfficialDiaryParse officialDiary = new OfficialDiaryParse()
                {
                     
                    Orgaos = new List<string>() {orgao1.Value,orgao2.Value,orgao3.Value},
                    CompleteExpression = all.Value,
                    Tipo = tipo.Value,
                    Norma = norma.Value,
                    NumeroNorma = numeroNorma.Value,
                    DataNorma = dataNorma.Value,
                    Content = content
                
                };
                listOfficialDiarys.Add(officialDiary);
       
            }

            return listOfficialDiarys;
        }
    }
}
