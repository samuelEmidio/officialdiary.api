﻿using OfficialDiary.Domain.Interfaces.Services.ParseDo;
using OfficialDiary.Domain.Entities;
using OfficialDiary.Domain.Enum;
using OfficialDiary.Domain.Interfaces.Services.ParseDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Application.Services.ParseDo
{
    public class ParseOfficialDiaryExecutePipelineService : IParseOfficialDiaryExecutePipelineService
    {
        private readonly IEnumerable<IParseOfficialDiaryService> _parseOfficialDiaries;

        public ParseOfficialDiaryExecutePipelineService(IEnumerable<IParseOfficialDiaryService> parseOfficialDiaries)
        {
            _parseOfficialDiaries = parseOfficialDiaries;
        }

        public List<OfficialDiaryParse> DoParse(string input, DoTypeEnum doType)
        {
            var implementation = _parseOfficialDiaries.FirstOrDefault(x => x.DoType == doType);
            return implementation.DoParse(input);
        }
    }
}
