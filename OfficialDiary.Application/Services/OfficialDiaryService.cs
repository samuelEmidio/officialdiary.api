﻿
using Newtonsoft.Json;
using OfficialDiary.Domain.Core.Entities;
using OfficialDiary.Domain.Interfaces.Repositories;
using OfficialDiary.Domain.Interfaces.Services;
using OfficialDiary.Domain.Interfaces.Services.ParseDo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Application.Services
{
    public class OfficialDiaryService : IOfficialDiaryService
    {
        private readonly IParseOfficialDiaryRepository _parseOfficialDiaryRepository;
        private readonly IOfficialDiaryRepository _officialDiaryRepository;
        private readonly IParseOfficialDiaryExecutePipelineService _parseOfficialDiaryExecutePipeline;

        public OfficialDiaryService(
            IParseOfficialDiaryExecutePipelineService parseOfficialDiaryExecutePipeline,
            IParseOfficialDiaryRepository parseOfficialDiaryRepository,
            IOfficialDiaryRepository officialDiaryRepository)
        {
            _parseOfficialDiaryExecutePipeline = parseOfficialDiaryExecutePipeline;
            _parseOfficialDiaryRepository = parseOfficialDiaryRepository;
            _officialDiaryRepository = officialDiaryRepository;
        }

        public async void ChangeDocument(string continuousChanges)
        {
            if (continuousChanges != null)
            {
                var _do = JsonConvert.DeserializeObject<DeserializeDocument<OfficialDiary.Domain.Entities.OfficialDiary>>(continuousChanges);

                if (_do.Doc.Attachments != null)
                {
                    var att = await _officialDiaryRepository.GetAttachment(_do.Doc.Id.ToString(), _do.Doc.Attachments.First().Key);

                    string result = System.Text.Encoding.UTF8.GetString(att.Content);

                    var listOfficialDiary = _parseOfficialDiaryExecutePipeline.DoParse(result, _do.Doc.Type);

                    listOfficialDiary.ForEach(x => x.OfficialDiaryId = _do.Doc.Id);

                    await _parseOfficialDiaryRepository.SetRepo("testeDb");
                    await _parseOfficialDiaryRepository.BulkInsert(listOfficialDiary);


                    Console.WriteLine("mundaça detectada");
                }
               
            }
        }
    }
}
