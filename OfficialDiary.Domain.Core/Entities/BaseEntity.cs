﻿using CouchDB.Driver.Types;
using MyCouch.Requests;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Domain.Core.Entities
{
    public abstract class BaseEntity
    {
        [JsonProperty("_id")]
        public Guid Id { get; protected set; } = Guid.NewGuid();
        public bool Active { get; set; } = true;
        public DateTime CreateAt { get; set; } = DateTime.Now;
        public DateTime? UpdateAt { get; set; }
    }
}
