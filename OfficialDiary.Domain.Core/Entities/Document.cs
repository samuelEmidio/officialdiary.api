﻿using CouchDB.Driver.Types;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Domain.Core.Entities
{
    public class Document : BaseEntity
    {
        [JsonProperty("_attachments")]
        public Dictionary<string, Attachment> Attachments;
    }
}
