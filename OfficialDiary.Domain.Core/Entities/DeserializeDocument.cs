﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Domain.Core.Entities
{
    public class DeserializeDocument<T> where T : class
    {
        public T Doc { get; set; }
    }
}
