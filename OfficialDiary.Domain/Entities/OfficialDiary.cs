﻿using OfficialDiary.Domain.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficialDiary.Domain.Core.Entities;

namespace OfficialDiary.Domain.Entities
{
    public class OfficialDiary : Document
    {
        public DoTypeEnum Type { get; set; }
    }
}
