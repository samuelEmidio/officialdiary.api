﻿using OfficialDiary.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Domain.Entities
{
    public class OfficialDiaryParse : Document
    {
        public List<string> Orgaos { get; set; }
        public string CompleteExpression { get; set; }
        public string Tipo { get; set; }
        public string Norma { get; set; }
        public string NumeroNorma { get; set; }
        public string DataNorma { get; set; }
        public string Content { get; set; }
        public Guid OfficialDiaryId { get; set; }
    }
}
