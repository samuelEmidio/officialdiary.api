﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Domain.Enum
{
    public enum DoTypeEnum
    {
        Dou = 0,
        Other = 1
    }
}
