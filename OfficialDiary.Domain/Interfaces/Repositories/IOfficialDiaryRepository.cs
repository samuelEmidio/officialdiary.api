﻿using CouchDB.Driver.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Domain.Interfaces.Repositories
{
    public interface IOfficialDiaryRepository : IBaseRepository<OfficialDiary.Domain.Entities.OfficialDiary>
    {

    }
}
