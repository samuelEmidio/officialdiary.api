﻿using CouchDB.Driver.Types;
using MyCouch.Requests;
using OfficialDiary.Domain.Entities;
using OfficialDiary.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Domain.Interfaces.Repositories
{
    public interface IParseOfficialDiaryRepository : IBaseRepository<OfficialDiaryParse>
    {
      
    }
}
