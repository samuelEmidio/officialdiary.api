﻿using MyCouch.Requests;
using MyCouch.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Domain.Interfaces.Repositories
{
    public interface IBaseRepository<TEntity> : IDisposable where TEntity : class
    {
        Task SetRepo(String db);
        Task<bool> Add(TEntity obj);
        Task<TEntity> GetById(string id);
        Task<bool> Update(TEntity obj);
        Task<bool> Remove(string id);
        Task<AttachmentResponse> GetAttachment(string docId, string docName);
        Task<bool> BulkInsert(List<TEntity> obj, int package = 100);
        void AddCallback(GetChangesRequest getChangesRequest, Action<string> callback);
    }
}
