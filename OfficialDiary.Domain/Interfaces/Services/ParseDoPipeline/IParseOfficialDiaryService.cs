﻿using OfficialDiary.Domain.Entities;
using OfficialDiary.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficialDiary.Domain.Interfaces.Services.ParseDo
{
    public interface IParseOfficialDiaryService
    {
        public DoTypeEnum DoType { get; set; }
        List<OfficialDiaryParse> DoParse(string input);
    }
}
