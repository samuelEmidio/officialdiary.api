﻿

namespace OfficialDiary.Domain.Interfaces.Services
{
    public interface IOfficialDiaryService
    {
        void ChangeDocument(string continuousChanges);
    }
}
