﻿using Microsoft.OpenApi.Models;
using OfficialDiary.Application.Services;
using OfficialDiary.Application.Services.ParseDo;
using OfficialDiary.Domain.Interfaces.Repositories;
using OfficialDiary.Domain.Interfaces.Services;
using OfficialDiary.Domain.Interfaces.Services.ParseDo;
using OfficialDiary.Infra.Data.DatabaseContext;
using OfficialDiary.Infra.Data.Repositories;

namespace OfficialDiary.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<CouchDbContext>();

            services.AddTransient<IParseOfficialDiaryRepository, ParseOfficialDiaryRepository>();
            services.AddTransient<IOfficialDiaryRepository, OfficialDiaryRepository>();


            services.AddTransient<IParseOfficialDiaryService, ParseOfficialDiaryOfTtheUnionService>();
            services.AddTransient<IParseOfficialDiaryExecutePipelineService, ParseOfficialDiaryExecutePipelineService>();
            

            services.AddTransient<IOfficialDiaryService, OfficialDiaryService>();



            services.AddTransient<IDbFeedChangesStartService, DbFeedChangesService>();

           


            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Dou.API", Version = "v1" });
            });

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Dou v1"));
            }

            

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var feedChangesServices = app.ApplicationServices.GetRequiredService<IDbFeedChangesStartService>();
            feedChangesServices.RegisterChanges();
        }

    }
}
