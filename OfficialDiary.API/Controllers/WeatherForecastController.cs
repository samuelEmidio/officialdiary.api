using OfficialDiary.Domain.Entities;
using OfficialDiary.Domain.Interfaces.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace OfficialDiary.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly IParseOfficialDiaryRepository _repository;
        private readonly IOfficialDiaryRepository _diaryRepository;

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, 
            IParseOfficialDiaryRepository parseOfficialDiaryRepository,
            IOfficialDiaryRepository officialDiaryRepository)
        {
            _logger = logger;
            _repository = parseOfficialDiaryRepository;
            _diaryRepository = officialDiaryRepository;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public async Task<IActionResult> GetAsync()
        {
            OfficialDiaryParse p = new OfficialDiaryParse()
            {
                Content = "teste"
            };

            await _repository.Add(p);

            OfficialDiary.Domain.Entities.OfficialDiary o = new Domain.Entities.OfficialDiary()
            {
                Type = Domain.Enum.DoTypeEnum.Other
            };

            await _diaryRepository.Add(o);

            return Ok();
        }

       
    }
}