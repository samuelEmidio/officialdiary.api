﻿using GetOfficialDiaryFunction.Entities;
using GetOfficialDiaryFunction.Enums;
using Microsoft.Extensions.Configuration;
using MyCouch;
using MyCouch.Net;
using MyCouch.Requests;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace GetOfficialDiaryFunction
{
    public class MainFunction
    {

        public static IConfigurationRoot configuration;

        public JObject Main(JObject args)
        {
            ConfigureAppsettings();

            Console.WriteLine("Processo Iniciado");


            SendDocumentToCouch().Wait();

            Console.WriteLine("Processo Finalizado");

            return args;
        }


        public async Task<bool> SendDocumentToCouch()
        {


            ConfigureAppsettings();
            Console.WriteLine("Busca info apppsettngs");
            var strConnection = configuration.GetSection("Cloudant").GetSection("Link").Value;
            var database = configuration.GetSection("Cloudant").GetSection("DatabaseOfficialDiary").Value;

            if (string.IsNullOrEmpty(strConnection) || string.IsNullOrEmpty(database))
                Console.WriteLine("appssetings falhou");
            else
                Console.WriteLine("appsetings capturado com sucesso");


            Console.WriteLine("Inicia conexao cloudant");
            DbConnectionInfo dbConnectionInfo = new DbConnectionInfo(strConnection, database);
            dbConnectionInfo.Timeout = TimeSpan.FromMilliseconds(System.Threading.Timeout.Infinite);
            MyCouchClient client = new MyCouchClient(dbConnectionInfo);

            var officialDiary = new OfficialDiary()
            {
                Active = true,
                CreateAt = DateTime.Now,
                UpdateAt = DateTime.Now,
                Type = DoTypeEnum.Dou
            };

            var json = client.DocumentSerializer.Serialize(officialDiary);
            var document = await client.Documents.PostAsync(json);
            Console.WriteLine("Documento salvo!");



            DirectoryInfo d = new DirectoryInfo(@Directory.GetCurrentDirectory()); //Assuming Test is your Folder

            FileInfo[] Files = d.GetFiles("*"); //Getting Text files
            string str = "";

            foreach (FileInfo file in Files)
            {
                Console.WriteLine(file.Name);
            }


            Console.WriteLine("Salvando attachment");
            var filexists = File.Exists(AppContext.BaseDirectory + @"/2021-03-01_1.pdf.txt");
            if (filexists)
                Console.WriteLine("xArquivo encontrado");
            else
                Console.WriteLine("yArquivo nao encontrado");

            var text = System.IO.File.ReadAllText(@Directory.GetCurrentDirectory() + @"/2021-03-01_1.pdf.txt", Encoding.UTF8);

            var base64 = EncodeTo64(text);

            var bytes = System.Convert.FromBase64String(base64);

            var requestAttachment = new PutAttachmentRequest(
                            document.Id, document.Rev,
                            "Official Diary",
                            HttpContentTypes.Jpeg,
                            bytes);
            var attachment = await client.Attachments.PutAsync(requestAttachment);

            Console.WriteLine("Attachment Salvo");

            return true;
        }




        private static void ConfigureAppsettings()
        {

            var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");

            configuration = builder.Build();

        }

        private string EncodeTo64(string toEncode)
        {

            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.UTF8.GetBytes(toEncode);

            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;

        }


    }
}
