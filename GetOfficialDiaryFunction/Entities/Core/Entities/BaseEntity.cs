﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GetOfficialDiaryFunction.Entities.Core.Entities
{
    public abstract class BaseEntity
    {
        [JsonProperty("_id")]
        public Guid Id { get; protected set; } = Guid.NewGuid();
        public bool Active { get; set; } = true;
        public DateTime CreateAt { get; set; } = DateTime.Now;
        public DateTime? UpdateAt { get; set; }
    }
}
