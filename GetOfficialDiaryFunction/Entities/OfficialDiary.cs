﻿using GetOfficialDiaryFunction.Entities.Core.Entities;
using GetOfficialDiaryFunction.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace GetOfficialDiaryFunction.Entities
{
    public class OfficialDiary : Document
    {
        public DoTypeEnum Type { get; set; }
    }
}
